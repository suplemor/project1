import React, {Component} from 'react'
import "../Common/Style/reset.css"
import "../Common/Style/base.css"

import Header from './Header/Header'
import Main from './Main/Main'
import Footer from './Footer/Footer'


class App extends Component {
State = {
  totalNews: 0,
}

  render () {
    return (
      <div>
      <Header/>
      <Main/>
      <Footer/> 
      </div>
   )
}
}

export default App
