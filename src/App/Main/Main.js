import React from 'react'
import {Link} from 'react-router-dom'
import AllNews from './AllNews/AllNews'
import './Main.css'

const Main = () => {
const getBackground = (news) => (
    {backgroundImage: `url(${news.image})`}
)
const routeList =  AllNews.map((news) => 
 
    <Link to="/blogPage" className="card medium" key={news.id}>
        <div className="card-wrapper">
            <figure className="cardImg" id="medium-fig1" style={getBackground(news)}></figure>
            <div className="cardImgOverlay"></div>
            <div className="card-title">
                <div className="post-info">
                    <p className="post-Tag">{news.category}</p>
                    <p className="post-Date">
                        <time dateTime="8 june">
                            Today
                        </time>
                    </p>
                </div>
                <h2>{news.articleName}</h2>
            </div>
            </div>
    </Link>

)

    return (
        <div>
        <div className='content_section content__feed' >
            {routeList}
        </div>
        <div className="load-more">
                <span>
                    Load more :)
                </span>
            </div>
        </div>
    )

}
export default Main

