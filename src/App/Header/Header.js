import React from 'react'
import './Header.css'

import HeaderLogo from "./HeaderLogo/HeaderLogo"
import HeaderNav from "./HeaderNav/HeaderNav"
import HeaderSearch from "./HeaderSearch/HeaderSearch"

const Header = () => {

    return (
        <header className="Page_Header">
        <section className="wrapper">
            
                <HeaderLogo/>
                <HeaderNav/>
                <HeaderSearch/>
           
        </section>
        </header>
    )

}

export default Header