import React from 'react'
import '../HeaderSearch/HeaderSearch.css'
const HeaderSearch = () => {

    return (
<div className="SerchMenu">
    <div className="searchButton">
        <button id="btn-header-search" className="btn btn--search">
            <span>
            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'>
                 <path fill='#1A1A1A' d='M11.318 9.858l3.945 3.945a1.037 1.037 0 0 1-1.467 1.466l-4.015-4.015a6.402 6.402 0 0 1-3.475 1.015C2.823 12.27 0 9.523 0 6.135 0 2.747 2.823 0 6.306 0c3.482 0 6.305 2.747 6.305 6.135 0 1.4-.482 2.69-1.293 3.723zm-5.012.51c2.403 0 4.352-1.895 4.352-4.233 0-2.339-1.949-4.234-4.352-4.234-2.404 0-4.352 1.895-4.352 4.234 0 2.338 1.948 4.233 4.352 4.233z'/>
            </svg>
            </span>
        </button>
        <button id="btn-header-search-close" className="btn btn--search-close">
            <span>
            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'>
                <path fill='#1A1A1A' d='M1.037 1.037l13.926 13.926L1.037 1.037zM8 6.534l6.23-6.23a1.037 1.037 0 0 1 1.466 1.466L9.466 8l6.23 6.23a1.037 1.037 0 0 1-1.466 1.466L8 9.466l-6.23 6.23A1.037 1.037 0 0 1 .304 14.23L6.534 8 .304 1.77A1.037 1.037 0 0 1 1.77.304L8 6.534z'/>
            </svg>
            </span>
        </button>
    </div>
    <div className="search" >
    <div className="search__layer">
        <form className="search__form" method="get" action="https://wylsa.com/">
            <div className="search__form-inner">
                <input type="search" id="search_content_button" placeholder="Search"/>
            </div>
        </form>
    </div>
    </div>
</div>
    )
}

export default HeaderSearch