import React from 'react'
import {NavLink} from 'react-router-dom'
import '../HeaderNav/HeaderNav.css'
const HeaderNav = () => {

    const routeMaps = [
        {
            value: "/Articles",
            text: "Articles"
        },
        {
            value: "/Reviews",
            text: "Reviews"
        },
        {
            value: "/News",
            text: "News"
        },
        {
            value: "/Poptag1",
            text: "Poptag1"
        },
        {
            value: "/Poptag2",
            text: "Poptag2"
        },
        
    ]
const routeList = routeMaps.map((route) => <li key={route.value}><NavLink exact to={route.value}>{route.text}</NavLink> </li>)

    return (

        <React.Fragment>

        <nav className="Header-menu-container">
            <ul className="header-menu">
                {routeList}
            </ul>
        </nav>
        </React.Fragment>
    )

}
export default HeaderNav