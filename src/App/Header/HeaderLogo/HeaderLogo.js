import React from "react"
import "../HeaderLogo/HeaderLogo.css"
import {Link} from 'react-router-dom'

const HeaderLogo = () => {
    return (
    <div className="logo">
        <Link to='/' className="logo-container">
            <div className="Header-logo">tech.Blog</div>
        </Link>  
    </div>
    )
}           
export default HeaderLogo