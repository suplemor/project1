import React from 'react'
import './FooterSocials.css'
import {Link} from 'react-router-dom'

const FooterSocials = () => {

    return (
<div className="social-buttons">
    <div className="social vk">
        <Link to="https://vk.com/" target="_blank"><i className="fa fa-vk fa-2x"></i></Link>    
    </div>
    <div className="social facebook">
        <Link to="https://www.facebook.com/" target="_blank"><i className="fa fa-facebook fa-2x"></i></Link>    
    </div>
    <div className="social instagram">
        <Link to="https://www.instagram.com/" target="_blank"><i className="fa fa-instagram fa-2x"></i></Link>
    </div>
    <div className="social youtube">
        <Link to="https://www.youtube.com/" target="_blank"><i className="fa fa-youtube fa-2x"></i></Link>
    </div>
</div>
    )
}

export default FooterSocials