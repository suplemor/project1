import React from "react"
import "./FooterLogo.css"
import {Link} from 'react-router-dom'

const FooterLogo = () => {
    return (
   
        <Link to='/' className="logo-container">
            <div className="Footer-logo">tech.Blog</div>
        </Link>  
 
    )
}           
export default FooterLogo
