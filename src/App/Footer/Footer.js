import React from 'react'
import './Footer.css'

import FooterLogo from "./FooterLogo/FooterLogo"
import FooterNav from "./FooterNav/FooterNav"
import FooterSocials from "./FooterSocials/FooterSocials"

const Footer = () => {

    return (
        <footer className="Page_Footer">
        <div className="wrapper">
           
                <FooterLogo/>
                <FooterNav/>
                <FooterSocials/>

        </div>
        </footer>
    )

}

export default Footer

