import React from 'react'
import {NavLink} from 'react-router-dom'
import './FooterNav.css'
const FooterNav = () => {

    const footerNavMap = [
        {
            value: "/AboutProject",
            text: "AboutProject"
        },  {
            value: "/Advertising",
            text: "Advertising"
        },  {
            value: "/Service",
            text: "Service"
        },  {
            value: "/Archive",
            text: "Archive"
        },
    ]
    
    const routeListFooterNav = footerNavMap.map((route) => <li key={route.value}><NavLink exact to={route.value}>{route.text}</NavLink></li>)
    return (
        <React.Fragment>
        <nav className="Footer-navigation">
            <ul className="Footer-menu">
                {routeListFooterNav}
            </ul>
        </nav>
        </React.Fragment>
    )

}
export default FooterNav